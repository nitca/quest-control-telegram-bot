"""
Database access and tables for mysql
"""

LOGIN = 'd0205toff'
PASSWORD = 'SmileInShadow7#'
DB = 'quests'
HOST = 'localhost'

USERS_TABLE = 'users'
GAME_TABLE = "game"
TASKS_TABLE = "tasks"
KEYS_TABLE = "secret_keys"

USERS_TABLE_DATA = "(" \
    "user_id int(11) unsigned NOT NULL DEFAULT 0 UNIQUE, "\
    "status tinyint NOT NULL DEFAULT 0, "\
    "perms tinyint NOT NULL DEFAULT 0, " \
    "points int unsigned NOT NULL DEFAULT 0"\
    ")"


GAME_TABLE_DATA = "(" \
    "user_id int(11) unsigned NOT NULL DEFAULT 0 UNIQUE, "\
    "numbers varchar(256) NOT NULL DEFAULT '', "\
    "cur_num tinyint NOT NULL DEFAULT 0 "\
    ")"


TASKS_TABLE_DATA = "(" \
    "id int AUTO_INCREMENT, "\
    "question TEXT NOT NULL, "\
    "answer TEXT NOT NULL, "\
    "photo TEXT NOT NULL, "\
    "PRIMARY KEY(id)"\
    ")"

KEYS_TABLE_DATA = "("\
    "secret_key int unsigned NOT NULL DEFAULT 0, "\
    "status tinyint NOT NULL DEFAULT 0"\
    ")"
