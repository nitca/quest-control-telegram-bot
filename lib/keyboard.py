# -*- coding: utf-8 -*-


from telebot.types import ReplyKeyboardMarkup, KeyboardButton
from .text_data import ButtonText
from .database_work import *


class Back:
    btn_back = KeyboardButton(ButtonText.back)
    kb_back = ReplyKeyboardMarkup(resize_keyboard=True)
    kb_back.add(btn_back)


class Admin:
    btn_create_game = KeyboardButton(ButtonText.admin_add_game)
    btn_mailing = KeyboardButton(ButtonText.admin_mailing)
    btn_control = KeyboardButton(ButtonText.admin_control)

    kb_main = ReplyKeyboardMarkup(resize_keyboard=True)
    kb_main.add(btn_create_game)
    kb_main.add(btn_mailing)
    kb_main.add(btn_control)

    btn_show_games = KeyboardButton(ButtonText.admin_show_game)
    btn_get_key = KeyboardButton(ButtonText.admin_get_key)

    btn_start_game = KeyboardButton(ButtonText.admin_start_game)
    kb_start_game = ReplyKeyboardMarkup(resize_keyboard=True)
    kb_start_game.add(btn_start_game)
    kb_start_game.add(btn_get_key)
    kb_start_game.add(btn_show_games)
    kb_start_game.add(Back.btn_back)

    btn_stop_game = KeyboardButton(ButtonText.admin_stop_game)
    btn_game_stat = KeyboardButton(ButtonText.game_stat)
    kb_stop = ReplyKeyboardMarkup(resize_keyboard=True)
    kb_stop.add(btn_get_key)
    kb_stop.add(btn_game_stat)
    kb_stop.add(btn_show_games)
    kb_stop.add(btn_stop_game)
    kb_stop.add(Back.btn_back)

    btn_stop_create = KeyboardButton(ButtonText.admin_stop_creation)
    kb_stop_create = ReplyKeyboardMarkup(resize_keyboard=True)
    kb_stop_create.add(btn_stop_create)

    def players_id_kb():
        table = UserTable()
        uids = table.get_all_players_id()
        kb = ReplyKeyboardMarkup(resize_keyboard=True)
        for uid in uids:
            tmp_btn = KeyboardButton(str(uid[0]))
            kb.add(tmp_btn)

        kb.add(Back.btn_back)
        return kb

    def tasks_id_kb():
        # GENERATE KEYBOARD FROM TASKS NUMBER
        # RETURNS 3 BUTTONS IN ROW
        table = TasksTable()
        tids = table.get_all_tasks()
        kb = ReplyKeyboardMarkup(resize_keyboard=True)

        tmp_buttons = []
        i = 1
        for tid in tids:
            tid = tid[0]
            tmp_buttons.append(str(tid))
            if i % 3 == 0:
                kb.add(tmp_buttons[0], tmp_buttons[1], tmp_buttons[2])
                tmp_buttons.clear()
            elif tid == tids[-1][0]:
                if len(tmp_buttons) == 2:
                    kb.add(tmp_buttons[0], tmp_buttons[1])
                else:
                    kb.add(tmp_buttons[0])
                tmp_buttons.clear()
            i += 1

        kb.add(Back.btn_back)
        return kb

    class Control:
        btn_edit_task = KeyboardButton(ButtonText.admin_control_edit)
        kb_control = ReplyKeyboardMarkup(resize_keyboard=True)
        kb_control.add(btn_edit_task)
        kb_control.add(Back.btn_back)

    class EditTask:
        btn_task = KeyboardButton(ButtonText.admin_edit_task)
        btn_answer = KeyboardButton(ButtonText.admin_edit_answer)
        btn_photo = KeyboardButton(ButtonText.admin_edit_photo)
        kb_edit = ReplyKeyboardMarkup(resize_keyboard=True)
        kb_edit.add(btn_task)
        kb_edit.add(btn_answer)
        kb_edit.add(btn_photo)
        kb_edit.add(Back.btn_back)


class Player:
    btn_get_key = KeyboardButton(ButtonText.player_start_game)
    btn_get_answer = KeyboardButton(ButtonText.player_enter_answer)

    kb_start_game = ReplyKeyboardMarkup(resize_keyboard=True)
    kb_start_game.add(btn_get_key)

    kb_enter_answer = ReplyKeyboardMarkup(resize_keyboard=True)
    kb_enter_answer.add(btn_get_answer)

    btn_game_stat = KeyboardButton(ButtonText.game_stat)
    kb_game_stat = ReplyKeyboardMarkup(resize_keyboard=True)
    kb_game_stat.add(btn_game_stat)
