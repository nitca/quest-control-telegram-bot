# -*- coding: utf-8 -*-

"""States, menus, keyboard text buttons"""


class States:
    """States for control users in menus"""
    admin_main_menu = "main_menu"
    admin_add_question = "admin_add_question"
    admin_add_answer = "admin_add_answer"
    admin_add_photo = "admin_add_photo"
    admin_stop_game = "admin_stop_game"
    admin_start_game = "admin_start_game"
    admin_mailing = "admin_mailing"
    admin_control = "admin_control"
    admin_control_states = "admin_control_states"
    admin_control_edit = "admin_control_edit"
    admin_control_ban = "admin_control_ban"

    admin_edit_task_menu = "admin_edit_task_menu"
    admin_edit_task_task = "admin_edit_task_task"
    admin_edit_task_answer = "admin_edit_task_answer"
    admin_edit_task_photo = "admin_edit_task_photo"

    player_no_game = "player_no_game"
    player_start_game = "player_start_game"
    player_enter_key = "player_enter_key"
    player_enter_answer = "player_enter_answer"


class MenuMessages:
    """All text messages in menu"""
    admin_main_menu = "Главное меню администратора"
    admin_add_question = "Введите подсказку:"
    admin_add_answer = "Введите ответ:"
    admin_add_photo = "Отправьте ссылку на тайника фото:"
    admin_stop_game = "Игра начата. Вы можете завершить её, "\
        "либо получить уникальный ключ для игрока."
    admin_stop_creating = "Вы можете завершить создание игры "\
        "или введите подсказку для добавления нового задания:"
    admin_game_created = "Игра успешно создана. "\
        "Вы можете её начать выбрав первое меню."
    admin_start_game = "Игра создана. Вы можете начать игру."
    admin_mailing = "Введите рассылаемое сообщение:"
    admin_control = "Меню управления"
    admin_control_states = "Список игроков"
    admin_control_edit = "Выберете номер загадки для редактирования:"
    admin_control_ban = "Выберите игрока, которого хотите заблокировать:"
    admin_control_edit = "Выберите номер загадки:"

    player_no_game = "В данные момент игр нет. "\
        "Мы Вас обязательно об них оповестим"
    player_game_started = "Игра начата! Чтобы присоединиться нажмите на кнопку"
    player_start_game = "Для подключения к игре необходим уникальный ключ.\n"\
        "Вы можете получить ключ у ведущего.\n\n"\
        "Введите ключ:"
    player_wrong_key = "Наверный ключ.\n\nВведите ключ:"
    player_enter_answer = "Введите ответ:"


class ButtonText:
    """Button text for keyboard"""
    admin_add_game = "Игра [🎉]"
    admin_stop_game = "Завершить игру [🛑]"
    admin_get_key = "Получить ключ [🔑]"
    admin_start_game = "Начать игру [🕹]"
    admin_show_game = "Просмотреть загадки [🗒]"
    admin_stop_creation = "Завершить создание игры [👍]"
    admin_mailing = "Рассылка [✉️]"
    admin_control = "Управление [🆔]"
    admin_control_states = "Статус игроков [🧩]"
    admin_control_edit = "Править головоломку [⚠️]"
    admin_control_ban = "Заблокировать игрока [🔓]"

    admin_edit_task = "Задача [❓]"
    admin_edit_answer = "Ответ [❗️]"
    admin_edit_photo = "Фото [🖼]"

    player_start_game = "Ввести ключ для подключения к игре [🔑]"
    player_enter_answer = "Ввести ответ [❗️]"

    game_stat = "Статус игры [🧩]"
    back = "Назад [⬅️]"
