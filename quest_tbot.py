# -*- coding: utf-8 -*-


from time import sleep
import telebot
import config
import lib.telebot_user_state as telebot_user_states
import lib.text_data as text_data
import lib.keyboard as kb
import lib.database_work as database_work
import lib.check_enter as check_enter


bot = telebot.TeleBot(config.TOKEN)

data_row = "question TEXT, answer TEXT, photo TEXT num INT"
user_states = telebot_user_states.UserStates()
user_data = telebot_user_states.UserData(data_row)

table_users = database_work.UserTable()
table_game = database_work.GameTable()
table_tasks = database_work.TasksTable()
table_keys = database_work.KeysTable()

# TURN ON/OFF CREATED GAME
table_keys.has_keys()


def admins_notify(msg, text):
    username = msg.chat.username
    if not username:
        username = str(msg.chat.id)

    text = text % username
    admins = table_users.get_all_players_id(1)
    for admin in admins:
        bot.send_message(admin[0], text)


def mailing(msg, game_started=False):
    users = table_users.get_all_players_id()
    admins = table_users.get_all_players_id(1)

    for user in users:
        try:
            if game_started:
                bot.send_message(
                    user[0], msg.text, reply_markup=kb.Player.kb_start_game
                )
                user_states.update_state(
                    user[0], text_data.States.player_start_game
                )

            else:
                if msg.photo is not None:
                    bot.send_photo(
                        user[0], msg.photo[0].file_id, msg.caption
                        )
                else:
                    bot.send_message(user[0], msg.text)

        except Exception as e:
            print(e)
            for admin in admins:
                message = f"Игрок {user[0]} заблокировал бота"
                bot.send_message(admin[0], message)


def back_to_main_menu(msg):
    perms = table_users.get_user_perms(msg.chat.id)
    if perms == 1:
        back_to_admin_main_menu(msg)
    elif perms == 0:
        back_to_player_main_menu(msg)


def back_to_admin_main_menu(msg):
    user_data.drop_data(msg.chat.id)
    user_states.update_state(msg.chat.id, text_data.States.admin_main_menu)
    user_data.add_user_in_data(msg.chat.id)
    bot.send_message(
        msg.chat.id,
        text_data.MenuMessages.admin_main_menu,
        reply_markup=kb.Admin.kb_main
        )


def back_to_admin_control_menu(msg):
    user_states.update_state(msg.chat.id, text_data.States.admin_control)
    bot.send_message(
        msg.chat.id,
        text_data.MenuMessages.admin_control,
        reply_markup=kb.Admin.Control.kb_control
        )


def back_to_player_main_menu(msg):
    if table_game.game_started:
        user_states.update_state(
            msg.chat.id, text_data.States.player_start_game
        )
        bot.send_message(
            msg.chat.id,
            text_data.MenuMessages.player_game_started,
            reply_markup=kb.Player.kb_start_game
            )
    else:
        user_states.update_state(
            msg.chat.id, text_data.States.player_no_game
        )
        bot.send_message(
            msg.chat.id,
            text_data.MenuMessages.player_no_game,
            reply_markup=telebot.types.ReplyKeyboardRemove()
            )


# START FUNC
@bot.message_handler(commands=['start'])
def start(msg):
    text = "Игрок %s запустил бота"
    admins_notify(msg, text)
    table_users.add_new_user(msg.chat.id)
    user_states.add_user(msg.chat.id)
    back_to_main_menu(msg)


# <-------------------------ADMIN BLOCK----------------------------->
# ADMIN MAIN MENU
@bot.message_handler(
    func=lambda msg: user_states.is_current_state(
        msg.chat.id, text_data.States.admin_main_menu
        )
    )
def admin_main_menu(msg):
    if msg.text == text_data.ButtonText.admin_add_game:
        # <--------------------IF GAME CREATED------------------------------->
        if table_keys.game_created:
            if table_game.game_started:
                bot.send_message(
                    msg.chat.id,
                    text_data.MenuMessages.admin_stop_game,
                    reply_markup=kb.Admin.kb_stop
                 )
                user_states.update_state(
                    msg.chat.id, text_data.States.admin_stop_game
                )
            else:
                bot.send_message(
                    msg.chat.id,
                    text_data.MenuMessages.admin_start_game,
                    reply_markup=kb.Admin.kb_start_game
                 )
                user_states.update_state(
                    msg.chat.id, text_data.States.admin_start_game
                    )

        # ADMIN START CREATION
        else:
            bot.send_message(
                msg.chat.id,
                text_data.MenuMessages.admin_add_question,
                reply_markup=kb.Back.kb_back
                )
            user_states.update_state(
                msg.chat.id, text_data.States.admin_add_question
            )

    elif msg.text == text_data.ButtonText.admin_mailing:
        bot.send_message(
            msg.chat.id,
            text_data.MenuMessages.admin_mailing,
            reply_markup=kb.Back.kb_back
        )
        user_states.update_state(msg.chat.id, text_data.States.admin_mailing)

    elif msg.text == text_data.ButtonText.admin_control:
        bot.send_message(
            msg.chat.id,
            text_data.MenuMessages.admin_control,
            reply_markup=kb.Admin.Control.kb_control
            )
        user_states.update_state(
            msg.chat.id, text_data.States.admin_control
        )


# ADMIN STOPS A GAME
@bot.message_handler(
    func=lambda msg: user_states.is_current_state(
        msg.chat.id, text_data.States.admin_stop_game
        )
    )
def admin_stop_game(msg):
    if msg.text == text_data.ButtonText.back:
        back_to_admin_main_menu(msg)
        return

    if msg.text == text_data.ButtonText.admin_stop_game:
        # CLEAR ALL GAME DATA
        table_game.drop_table()
        table_tasks.drop_table()
        table_keys.drop_table()
        message = "Игра закончена. Надеюсь вы приятно провели время 😊\n\n "\
            "Ждём вас в следующей игре. Ожидайте оповещений 😉"
        mailing(message)
        bot.send_message(msg.chat.id, "Игра закрыта. Таблицы игры очищены")
        back_to_admin_main_menu(msg)

    elif msg.text == text_data.ButtonText.admin_get_key:
        key = table_keys.get_uniqie_key()
        bot.send_message(msg.chat.id, key)

    elif msg.text == text_data.ButtonText.game_stat:
        stat, players = table_game.get_game_data(config.AMOUNT, True)
        bot.send_message(msg.chat.id, stat)

        for player in players:
            message = f"Ид игрока: {player[0]}\nНомер задания: {player[1]}"
            bot.send_message(msg.chat.id, message)

    elif msg.text == text_data.ButtonText.admin_show_game:
        lines = table_tasks.get_all_tasks()
        if lines[0][0] is None:
            bot.send_message(msg.chatid, "Заданий не обнаружено!")
        else:
            for line in lines:
                message = f"Задание:\n{line[1]}\n\nКлюч: {line[2]}\n"
                bot.send_photo(msg.chat.id, line[3], message)


# ADMIN START GAME
@bot.message_handler(
    func=lambda msg: user_states.is_current_state(
        msg.chat.id, text_data.States.admin_start_game
        )
    )
def admin_start_game(msg):
    if msg.text == text_data.ButtonText.back:
        back_to_admin_main_menu(msg)
        return

    if msg.text == text_data.ButtonText.admin_start_game:
        table_game.max_number = table_tasks.get_totalnum_tasks()
        table_game.game_started = True
        mailing(text_data.MenuMessages.player_game_started, game_started=True)
        bot.send_message(msg.chat.id, "Игра начата!")
        back_to_admin_main_menu(msg)

    elif msg.text == text_data.ButtonText.admin_get_key:
        key = table_keys.get_uniqie_key()
        bot.send_message(msg.chat.id, key)

    elif msg.text == text_data.ButtonText.admin_show_game:
        lines = table_tasks.get_all_tasks()
        if lines[0][0] is None:
            bot.send_message(msg.chatid, "Заданий не обнаружено!")
        else:
            for line in lines:
                message = f"Задание:\n{line[1]}\n\nКлюч: {line[2]}\n"
                bot.send_photo(msg.chat.id, line[3], message)


# ADMIN ADDITION NEW TAST 1 STEP
@bot.message_handler(
    func=lambda msg: user_states.is_current_state(
        msg.chat.id, text_data.States.admin_add_question
        )
    )
def admin_add_question(msg):
    if msg.text == text_data.ButtonText.back:
        back_to_admin_main_menu(msg)
        return

    # IF ADMIN STOPS CREATION'S GAME
    if msg.text == text_data.ButtonText.admin_stop_creation:
        table_keys._create_table()
        bot.send_message(
            msg.chat.id, "Пожалуйста, подождите генерации уникальных ключей"
            )
        table_keys.generate_keys()

        table_game._create_table()
        table_keys.game_created = True
        bot.send_message(
            msg.chat.id, text_data.MenuMessages.admin_game_created
        )
        back_to_admin_main_menu(msg)
        return

    user_data.add_data(msg.chat.id, 'question', msg.text)
    bot.send_message(
        msg.chat.id,
        text_data.MenuMessages.admin_add_answer,
        reply_markup=telebot.types.ReplyKeyboardRemove()
        )
    user_states.update_state(msg.chat.id, text_data.States.admin_add_answer)


# 2 STEP
@bot.message_handler(
    func=lambda msg: user_states.is_current_state(
        msg.chat.id, text_data.States.admin_add_answer
        )
    )
def admin_add_answer(msg):
    if msg.text == text_data.ButtonText.back:
        back_to_admin_main_menu(msg)
        return
    user_data.add_data(msg.chat.id, 'answer', msg.text)
    bot.send_message(msg.chat.id, text_data.MenuMessages.admin_add_photo)
    user_states.update_state(msg.chat.id, text_data.States.admin_add_photo)


# 3 STEP
@bot.message_handler(
    func=lambda msg: user_states.is_current_state(
        msg.chat.id, text_data.States.admin_add_photo
        )
    )
def admin_add_photo(msg):
    if msg.text == text_data.ButtonText.back:
        back_to_admin_main_menu(msg)
        return

    if check_enter.link(msg.text):
        data = user_data.get_all_data_by_id(msg.chat.id)
        table_tasks.create_table()
        table_tasks.add_task(data[1], data[2], msg.text)
        message = f"Добавлено ✅\nЗадание: {data[1]}\n\nОтвет: {data[2]}\n"
        bot.send_photo(msg.chat.id, msg.text, message)
        bot.send_message(
            msg.chat.id,
            text_data.MenuMessages.admin_stop_creating,
            reply_markup=kb.Admin.kb_stop_create

        )
        user_states.update_state(
            msg.chat.id, text_data.States.admin_add_question
            )
    else:
        bot.send_message(msg.chat.id, "Что-то пошло не так. Проверьте ссылку")
        bot.send_message(msg.chat.id, text_data.MenuMessages.admin_add_photo)


# ADMIN MAILING
@bot.message_handler(
    func=lambda msg: user_states.is_current_state(
        msg.chat.id, text_data.States.admin_mailing
        ),
    content_types=['text', 'photo']
    )
def admin_mailing(msg):
    if msg.text == text_data.ButtonText.back:
        back_to_admin_main_menu(msg)
        return

    mailing(msg)

    bot.send_message(msg.chat.id, "Сообщение разослано всем игрокам")
    back_to_admin_main_menu(msg)


@bot.message_handler(
    func=lambda msg: user_states.is_current_state(
        msg.chat.id, text_data.States.admin_control
            )
        )
def admin_control(msg):
    if msg.text == text_data.ButtonText.back:
        back_to_admin_main_menu(msg)
        return

    if msg.text == text_data.ButtonText.admin_control_edit:
        if table_keys.game_created:
            bot.send_message(
                msg.chat.id,
                text_data.MenuMessages.admin_control_edit,
                reply_markup=kb.Admin.tasks_id_kb()
            )
            user_states.update_state(
                msg.chat.id, text_data.States.admin_control_edit
            )
        else:
            bot.send_message(msg.chat.id, "Игра пока не создана")

# <-----------------ADMIN EDIT TASK BLOCK------------------------------>


@bot.message_handler(
    func=lambda msg: user_states.is_current_state(
        msg.chat.id, text_data.States.admin_control_edit
            )
        )
def admin_edit_task(msg):
    if msg.text == text_data.ButtonText.back:
        back_to_admin_control_menu(msg)
        return

    if check_enter.task_exists(msg.text):
        user_data.add_data(msg.chat.id, 'question', msg.text)
        num_task = int(msg.text)
        task_data = table_tasks.get_task_byid(num_task)
        message = f"Задание:\n{task_data[1]}\nОтвет:{task_data[2]}"
        bot.send_photo(msg.chat.id, task_data[3], message)
        bot.send_message(
            msg.chat.id,
            "Что правим? Задание, ответ или фото?",
            reply_markup=kb.Admin.EditTask.kb_edit
            )
        user_states.update_state(
            msg.chat.id, text_data.States.admin_edit_task_menu
            )
    else:
        bot.send_message(msg.chat.id, "Проверьте ввод")
        bot.send_message(
            msg.chat.id, text_data.MenuMessages.admin_control_edit
        )


@bot.message_handler(
    func=lambda msg: user_states.is_current_state(
        msg.chat.id, text_data.States.admin_edit_task_menu
        )
    )
def admin_edit_task_menu(msg):
    if msg.text == text_data.ButtonText.back:
        back_to_admin_control_menu(msg)
        return

    if msg.text == text_data.ButtonText.admin_edit_task:
        bot.send_message(
            msg.chat.id, "Введите новое задание:", reply_markup=kb.Back.kb_back
            )
        user_states.update_state(
            msg.chat.id, text_data.States.admin_edit_task_task
            )
    elif msg.text == text_data.ButtonText.admin_edit_answer:
        bot.send_message(
            msg.chat.id, "Введите новый ответ:", reply_markup=kb.Back.kb_back
            )
        user_states.update_state(
            msg.chat.id, text_data.States.admin_edit_task_answer
            )
    elif msg.text == text_data.ButtonText.admin_edit_photo:
        bot.send_message(
            msg.chat.id,
            "Отправьте ссылку на новое фото:",
            reply_markup=kb.Back.kb_back
            )
        user_states.update_state(
            msg.chat.id, text_data.States.admin_edit_task_photo
            )


# EDIT TASK, ANSWER OR PHOTO
@bot.message_handler(
    func=lambda msg: user_states.is_current_state(
        msg.chat.id,
        text_data.States.admin_edit_task_task,
        text_data.States.admin_edit_task_answer,
        text_data.States.admin_edit_task_photo
        )
    )
def admin_edit_text_task(msg):
    if msg.text == text_data.ButtonText.back:
        back_to_admin_control_menu(msg)
        return

    ustate = user_states.get_current_state(msg.chat.id)
    num_task = user_data.get_all_data_by_id(msg.chat.id)[1]
    num_task = int(num_task)

    if ustate == text_data.States.admin_edit_task_task:
        table_tasks.update_task(num_task, 'question', msg.text)
        task_data = table_tasks.get_task_byid(num_task)
        message = f"Задача:\n{task_data[1]}\nОтвет: {task_data[2]}"
        bot.send_photo(msg.chat.id, task_data[3], message)
        back_to_admin_control_menu(msg)

    elif ustate == text_data.States.admin_edit_task_answer:
        table_tasks.update_task(num_task, 'answer', msg.text)
        task_data = table_tasks.get_task_byid(num_task)
        message = f"Задача:\n{task_data[1]}\nОтвет: {task_data[2]}"
        bot.send_photo(msg.chat.id, task_data[3], message)
        back_to_admin_control_menu(msg)
    elif ustate == text_data.States.admin_edit_task_photo:
        if check_enter.link(msg.text):
            table_tasks.update_task(num_task, 'photo', msg.text)
            task_data = table_tasks.get_task_byid(num_task)
            message = f"Задача:\n{task_data[1]}\nОтвет: {task_data[2]}"
            bot.send_photo(msg.chat.id, task_data[3], message)
            back_to_admin_control_menu(msg)
        else:
            bot.send_message(msg.chat.id, "Наверная ссылка")
            bot.send_message(msg.chat.id, "Отправьте новую ссылку фото:")
# <-----------------ADMIN EDIT TASK END------------------------------>

# </-------------------------ADMIN BLOCK----------------------------->


# <-------------------------PLAYER BLOCK----------------------------->
@bot.message_handler(
    func=lambda msg: user_states.is_current_state(
        msg.chat.id, text_data.States.player_start_game
        )
    )
def player_start_game(msg):
    if msg.text == text_data.ButtonText.player_start_game:
        bot.send_message(
            msg.chat.id,
            text_data.MenuMessages.player_start_game,
            reply_markup=kb.Back.kb_back
            )
        user_states.update_state(
            msg.chat.id, text_data.States.player_enter_key
            )


# PLAYER ENTER KEY
@bot.message_handler(
    func=lambda msg: user_states.is_current_state(
        msg.chat.id, text_data.States.player_enter_key
            )
    )
def player_enter_key(msg):
    if msg.text == text_data.ButtonText.back:
        back_to_player_main_menu(msg)

    if table_keys.check_key(msg.text):
        text = "Игрок %s подключился к игре"
        admins_notify(msg, text)

        table_game.add_user(msg.chat.id)
        task = table_tasks.get_task(msg.chat.id)

        if task[0] is not None:
            message = f"Номер загадки: {task[0]}. Загадка:\n{task[1]}"
            bot.send_message(
                msg.chat.id, "Вы подключены к игре! Первая загадка:"
                )
            bot.send_message(
                msg.chat.id,
                message,
                reply_markup=telebot.types.ReplyKeyboardRemove()
            )
            user_states.update_state(
                msg.chat.id, text_data.States.player_enter_answer
                )
            bot.send_message(
                msg.chat.id,
                text_data.MenuMessages.player_enter_answer,
                reply_markup=kb.Player.kb_game_stat
            )
        else:
            bot.send_message(msg.chat.id, "Все загадки отгаданы!")
    else:
        bot.send_message(
            msg.chat.id, text_data.MenuMessages.player_wrong_key
            )


@bot.message_handler(
    func=lambda msg: user_states.is_current_state(
        msg.chat.id, text_data.States.player_enter_answer
        )
    )
def player_answer(msg):
    if msg.text == text_data.ButtonText.game_stat:
        stat = table_game.get_game_data(config.AMOUNT)
        bot.send_message(msg.chat.id, stat)
        return

    task = table_tasks.get_task(msg.chat.id)
    if task[0] is None:
        bot.send_message(
            msg.chat.id,
            "Вы отгадали последнюю загадку! Скорее отправляйтесь к ведущему!"
            )
        return

    elif msg.text == task[2]:
        text = f"Игрок %s завершил {task[0]} задание"
        admins_notify(msg, text)
        bot.send_message(msg.chat.id, "Верно! Следующая загадка:")
        bot.send_photo(
            msg.chat.id,
            task[3],
            "Обязательно расположите ключ точно как на фото"
            )

        sleep(10)
        table_game.update_user_number(msg.chat.id)
        task = table_tasks.get_task(msg.chat.id)
        if task[0] is None:
            bot.send_message(
                msg.chat.id,
                "Вы отгадали последнюю загадку! Скорее "
                "отправляйтесь к ведущему!"
            )
            return
        else:
            message = f"Номер загадки: {task[0]}. Загадка:\n{task[1]}"
            bot.send_message(msg.chat.id, message)

    else:
        bot.send_message(msg.chat.id, "Похоже что Вы ввели что-то не то")


bot.infinity_polling(timeout=120)
