import re
from .database_work import *


table_users = UserTable()
table_tasks = TasksTable()


def link(url):
    pattern = re.compile(
        r'^https?:\/\/[a-zA-Z0-9,_,.]{0,}\/[\w,\d,\/, -]{0,}\.[a-z]{3,4}'
        )
    if re.findall(pattern, url):
        return True
    return False


def task_exists(task_id):
    try:
        task_id = int(task_id)
    except Exception:
        return False

    data = table_tasks.get_task_byid(task_id)
    if data[0] is None:
        return False
    return True
