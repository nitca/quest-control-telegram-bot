# -*- coding: utf-8 -*-


import random
from .masterclass_database import *
from .db_data import *


def generate_unique_keys(total=30):
    i = 0
    while i < total:
        number = random.randint(10000000, 99999999)
        i += 1
        yield number


class UserTable(WorkDB):
    def __init__(self):
        self.table = USERS_TABLE
        self.cols = USERS_TABLE_DATA
        super().__init__(self.table, self.cols)

    def add_new_user(self, user_id):
        query = f"INSERT IGNORE INTO {self.table} (user_id) VALUES({user_id})"
        self._execute_query_without_return(query)

    def get_user_status(self, user_id):
        query = f"SELECT status FROM {self.table} WHERE user_id={user_id}"
        data = self._execute_query_with_return(query)
        try:
            return data[0][0]
        except Exception as e:
            print(e)
            return 0

    def get_user_perms(self, user_id):
        query = f"SELECT perms FROM {self.table} WHERE user_id={user_id}"
        data = self._execute_query_with_return(query)
        try:
            return data[0][0]
        except Exception as e:
            print(e)
            return 0

    def update_user_status(self, user_id, status):
        query = f"UPDATE {self.table} SET status={status} "\
            f"WHERE user_id={user_id}"
        self._execute_query_without_return(query)

    def update_user_perms(self, user_id, perms):
        query = f"UPDATE {self.table} SET perms={perms} "\
            f"WHERE user_id={user_id}"
        self._execute_query_without_return(query)

    def get_all_players_id(self, perms=0):
        query = f"SELECT user_id FROM {self.table} WHERE perms={perms}"
        data = self._execute_query_with_return(query)
        return data

    def increase_points(self, user_id):
        query = "UPDATE {self.table} set points=points+10"\
            f"WHERE user_id={user_id}"
        self._execute_query_without_return(query)


class TasksTable(WorkDB):
    def __init__(self):
        self.table = TASKS_TABLE
        self.cols = TASKS_TABLE_DATA
        super().__init__(self.table, self.cols, False)

    def create_table(self):
        self.game_created = True
        self._create_table()

    def add_task(self, question, answer, photo):
        query = f"INSERT INTO {self.table} (question, answer, photo) "\
            f"VALUES('{question}', '{answer}', '{photo}')"
        self._execute_query_without_return(query)

    def get_task(self, user_id):
        query = f"SELECT * FROM {self.table} WHERE "\
            f"id=(SELECT cur_num FROM game WHERE user_id={user_id}) LIMIT 1"
        data = self._execute_query_with_return(query)
        try:
            return data[0]
        except Exception:
            return (None, )

    def get_all_tasks(self):
        query = f"SELECT * FROM {self.table}"
        data = self._execute_query_with_return(query)
        try:
            return data
        except Exception:
            return ((None, ),)

    def delete_task(self, task_id):
        query = f"DELETE FROM TABLE {self.table} WHERE id={task_id}"
        self._execute_query_without_return(query)

    def get_task_byid(self, id):
        query = f"SELECT * FROM {self.table} where id={id}"
        try:
            data = self._execute_query_with_return(query)
            return data[0]
        except Exception:
            return (None, )

    def get_totalnum_tasks(self):
        query = f"SELECT COUNT(*) FROM {self.table}"
        data = self._execute_query_with_return(query)
        return data[0][0]

    def update_task(self, id, key, data):
        query = f"UPDATE {self.table} SET {key}='{data}' WHERE id={id}"
        self._execute_query_without_return(query)

    def drop_table(self):
        query = f"DROP TABLE {self.table}"
        self._execute_query_without_return(query)


class GameTable(WorkDB):
    def __init__(self):
        self.table = GAME_TABLE
        self.cols = GAME_TABLE_DATA
        self.game_started = False
        self.max_number = 0
        super().__init__(self.table, self.cols, False)

    def add_user(self, user_id):
        nums = range(1, self.max_number)
        cur_num = random.choice(nums)
        numbers = str(cur_num) + ' '
        i = 0
        while i < self.max_number - 4:
            num = str(random.choice(nums))
            if num not in numbers:
                numbers += num + ' '
                i += 1
        numbers = numbers[:-1]

        query = f"INSERT IGNORE INTO {self.table} (user_id, numbers, cur_num)"\
            f" VALUES({user_id}, '{numbers}', {cur_num})"
        self._execute_query_without_return(query)

    def get_user_number(self, user_id):
        query = f"SELECT cur_num FROM {self.table} WHERE user_id={user_id}"
        data = self._execute_query_with_return(query)
        try:
            return data[0][0]
        except Exception as e:
            print(e)
            return 0

    def update_user_number(self, user_id):
        cur_num = self.get_user_number(user_id)
        query = f"SELECT numbers FROM {self.table} where user_id={user_id}"
        numbers = self._execute_query_with_return(query)[0][0]

        try:
            numbers = numbers.split()
            numbers.remove(str(cur_num))
        except ValueError:
            cur_num = 0

        try:
            cur_num = numbers[random.randint(0, len(numbers)-1)]
        except ValueError:
            try:
                cur_num = numbers[0]
            except IndexError:
                cur_num = 0

        numbers = ' '.join(numbers)

        query = f"UPDATE {self.table} SET cur_num={cur_num}, "\
            f"numbers='{numbers}' WHERE user_id={user_id}"
        self._execute_query_without_return(query)

    def get_game_data(self, amount, admin=False):
        query = f"SELECT COUNT(*) FROM {self.table}"
        players = self._execute_query_with_return(query)[0][0]
        amount = amount * players
        message = "Игра создана\n\n"\
            "По всем вопросам пишите: @ni_nitca\n\n"\
            f"Всего игроков: {players}\nСумма в банке: {amount} тенге"
        if admin:
            query = f"SELECT * FROM {self.table}"
            players = self._execute_query_with_return(query)
            return message, players
        else:
            return message

    def drop_table(self):
        self.game_started = False
        query = f"DROP TABLE {self.table}"
        self._execute_query_without_return(query)


class KeysTable(WorkDB):
    def __init__(self):
        self.table = KEYS_TABLE
        self.cols = KEYS_TABLE_DATA

        # IF HAS KEYS THEN GAME CREATED
        self.game_created = False

        super().__init__(self.table, self.cols, False)

    def get_uniqie_key(self):
        query = f"SELECT secret_key FROM {self.table} WHERE status=0 LIMIT 1"
        data = self._execute_query_with_return(query)
        try:
            skey = data[0][0]
            query = f"UPDATE {self.table} SET status=1 WHERE secret_key={skey}"
            self._execute_query_without_return(query)
            return skey
        except Exception as e:
            print(e)
            return 0

    def check_key(self, key):
        query = f"SELECT secret_key FROM {self.table} WHERE secret_key={key}"
        try:
            self._execute_query_with_return(query)[0][0]
            self.delete_key(key)
            return True
        except Exception:
            return False

    def delete_key(self, key):
        query = f"DELETE FROM {self.table} WHERE secret_key={key}"
        self._execute_query_without_return(query)

    def generate_keys(self):
        gen_keys = generate_unique_keys(30)
        for key in gen_keys:
            query = f"INSERT INTO {self.table} (secret_key) VALUES({key})"
            self._execute_query_without_return(query)

    def has_keys(self):
        query = f"SELECT COUNT(*) FROM {self.table}"
        try:
            self._execute_query_with_return(query)[0][0]
            self.game_created = True
        except Exception:
            self.game_created = False

    def drop_table(self):
        self.game_created = False
        query = f"DROP TABLE {self.table}"
        self._execute_query_without_return(query)
